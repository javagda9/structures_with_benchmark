package com.sda.structures;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Benchmark {

    public static void main(String[] args) {
//        List<Integer> lista = new LinkedList<Integer>();
//        List<Integer> lista = new ArrayList<Integer>();

        // ###################################
        // praca na pustej liście - dodawanie
        for (int i = 0; i < 1; i++) {
//            List<Integer> lista = new ArrayList<Integer>(10000002);
//            addEnd(lista, 1000000);
            // średnio  309503021
        }
//        for (int i = 0; i < 10; i++) {
//            LinkedList<Integer> lista = new LinkedList<Integer>();
//            addEndLinked(lista, 10000000);
//            // średnio 1219467321
//        }
        // ###################################

        // ###################################
//        for (int i = 0; i < 10; i++) {
//            get(lista, lista.size()/2, 1000);
//        }
        // 13185308517
        // ###################################


        // ###################################
        // dodanie na początek
//        for (int i = 0; i < 10; i++) {
//            addAt(lista, 0, 1000);
//        }// array
        // ###################################


        // ###################################
        // usunięcie z początku
        for (int i = 0; i < 20; i++) {
            LinkedList<Integer> lista = new LinkedList<Integer>();
            addEnd(lista, 1000000);
            try {
                Runtime.getRuntime().gc(); // sprzątanie
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //linked: 636087
            removeLast(lista, 10000);
        }//
        // ###################################
    }

    //1. metoda która jako parametr przyjmuje Listę, indeks oraz ilość elementów, a następnie do listy dodaje na danym indeksie podaną ilość razy element '0'
    public static void addAt(List<Integer> list, int index, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.add(index, 0);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("addAt: " + (timeFinish - time));
    }

    //2. metoda która jako parametr przyjmuje Listę, ilość elementów, a następnie do listy dodaje na końcu podaną ilość razy element '0'
    public static void addEnd(List<Integer> list, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.add(0);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("addEnd: " + (timeFinish - time));
    }

    public static void addEndLinked(LinkedList<Integer> list, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.addLast(0);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("addEnd: " + (timeFinish - time));
    }

    //3. metoda która jako parametr przyjmuje Listę, ilość elementów, a następnie do listy na początku dodaje odaną ilość razy element '0'
    public static void addBegin(List<Integer> list, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.add(0);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("addBegin: " + (timeFinish - time));
    }

    //4. metoda która jako parametr przyjmuje listę, ilość powtórzeń, numer elementu, a następnie z listy wykonuje pobranie ('get') podanego elementu zadaną ilość razy
    public static void get(List<Integer> list, int index, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.get(index);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("get: " + (timeFinish - time));
    }

    //5. metoda która jako parametr przyjmuje listę, ilość powtórzeń, numer elementu, a następnie z listy wykonuje usunięcie ('remove') podanego elementu zadaną ilość razy
    public static void remove(List<Integer> list, int index, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.remove(index);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("remove: " + (timeFinish - time));
    }

    //6. metoda która jako parametr przyjmuje listę, ilość powtórzeń, a następnie z listy wykonuje usunięcie ('remove') ostatniego elementu zadaną ilość razy
    public static void removeLast(List<Integer> list, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.remove(list.size() - 1);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("removeLast: " + (timeFinish - time));
    }
    public static void removeLastLinked(LinkedList<Integer> list, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.removeLast();
        }
        Long timeFinish = System.nanoTime();
        System.out.println("removeLast: " + (timeFinish - time));
    }

    //7. metoda która jako parametr przyjmuje listę, ilość powtórzeń, a następnie z listy wykonuje usunięcie ('remove') pierwszego elementu zadaną ilość razy
    public static void removeFirst(List<Integer> list, int howmany) {
        Long time = System.nanoTime();

        for (int i = 0; i < howmany; i++) {
            list.remove(0);
        }
        Long timeFinish = System.nanoTime();
        System.out.println("removeFirst: " + (timeFinish - time));
    }
}
