package com.sda.structures.ll;

public class Element<T> {
    private Element<T> next;
    private Element<T> prev;

    private T data;

    public Element(T data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }

    public Element<T> getNext() {
        return next;
    }

    public void setNext(Element<T> next) {
        this.next = next;
    }

    public Element<T> getPrev() {
        return prev;
    }

    public void setPrev(Element<T> prev) {
        this.prev = prev;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
