package com.sda.structures.ll;

public class MyLinkedList<E> {
    private int size;

    private Element<E> head;
    private Element<E> tail;

    public MyLinkedList() {
        this.size = 0;
        this.head = null;
        this.tail = null;
    }

    public void add(E data) {
        Element<E> newElement = new Element(data);
        if (this.head == null && this.tail == null) { // lista jest pusta i dodajemy 1 element
            this.head = newElement;
            this.tail = newElement;
        } else {
            this.tail.setNext(newElement); // ostatni element ma jako nastepnik nowy element
            newElement.setPrev(this.tail); // nowy element ma jako poprzednik ostatni
            this.tail = newElement;     // przestawiamy ostatni. Ostatnim jest teraz nowo dodany element
        }
        this.size++;
    }

    public void print() {
        if (size <= 0) {
            return;
        }
        Element<E> tmp = this.head;

        System.out.print(tmp.getData() + " ");
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            System.out.print(tmp.getData() + " ");
        }
    }

    public void remove(int indeks) {
        if (indeks < 0 || indeks > size) {
            throw new ArrayIndexOutOfBoundsException();
        }

        Element<E> tmp = head;
        int counter = 0;
        while (counter < indeks && tmp.getNext() != null) {
            tmp = tmp.getNext();
            counter++;
        }

        if (tmp.getPrev() != null) { // zabezpieczenie przy usuwaniu 1 elementu
            tmp.getPrev().setNext(tmp.getNext()); //1
        }

        if (tmp.getNext() != null) { // zabezpieczenie przy usuwaniu ostatniego
            tmp.getNext().setPrev(tmp.getPrev()); //2
        }

        if (tmp == head) { // jeśli usuwam head
            this.head = tmp.getNext();
        }

        if (tmp == tail) { // jeśli usuwam tail
            this.tail = tmp.getPrev();
        }
        this.size--;
    }

    public void removeLast() {
        tail.getPrev().setNext(null);
        tail = tail.getPrev();
    }

    public void removeFirst() {
        head.getNext().setPrev(null);
        head = head.getNext();
    }

    public E get(int indeks) {
        if (indeks < 0 || indeks >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }

        Element<E> tmp = head;
        int counter = 0;
        while (counter <= indeks) {
            if (counter == indeks) {
                return tmp.getData();
            }
            tmp = tmp.getNext();
            counter++;
        }
        // nigdy nie powinno dojść do tego miejsca
        return null;
    }

    public void add(int indeks, E value) {
        if (indeks < 0 || indeks > size) {
            // wykraczamy poza listę
            throw new ArrayIndexOutOfBoundsException();
        } else if (indeks == size) {
            // dodanie na koniec
            add(value);
            return;
        }
        Element<E> newElement = new Element(value);

        Element<E> tmp = head;
        int counter = 0;
        while (counter < indeks && tmp.getNext() != null) {
            tmp = tmp.getNext();
            counter++;
        }

        // dodawanie gdzieś w środku
        //tmp// element o numerze indeks
        if (tmp.getPrev() != null) {
            tmp.getPrev().setNext(newElement);
        }
        newElement.setPrev(tmp.getPrev());

        tmp.setPrev(newElement);
        newElement.setNext(tmp);

        if (indeks == 0) {
            head = newElement;
        }
        size++;
    }

    public int find(E value) {
        Element<E> tmp = head;
        int counter = 0;
        while (tmp != null) {
            // żeby pętla zwrociła ostatni element to zamiast tmp.getNext() mam tmp
            if (tmp.getData() == value) {
                return counter;
            }
            tmp = tmp.getNext();
            counter++;
        }

        return -1;
    }

    public boolean contains(E value) {
        Element<E> tmp = head;
        int counter = 0;
        while (tmp != null) {
            // żeby pętla zwrociła ostatni element to zamiast tmp.getNext() mam tmp
            if (tmp.getData() == value) {
                return true;
            }
            tmp = tmp.getNext();
            counter++;
        }

        return false;
    }

    public void set(int index, E value) {
        if (index < 0 || index > size) {
            // wykraczamy poza listę
            throw new ArrayIndexOutOfBoundsException();
        }

        Element<E> tmp = head;
        int counter = 0;
        while (counter <= index && tmp != null) {
            if(counter == index){
                tmp.setData(value);
                return;
            }
            tmp = tmp.getNext();
            counter++;
        }
    }


    public void addAll(E... objects) {
        for (int i = 0; i < objects.length; i++) {
            // korzystamy z istniejącej już metody dodania
            add(objects[i]);
        }
    }

    public void clear() {
        this.head = null;
        this.tail = null;
        size = 0;
    }
}
