package com.sda.structures.ll;

import java.util.ArrayList;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {

        MyLinkedList myLinkedList = new MyLinkedList();

        myLinkedList.add(1);
        myLinkedList.add(2);
        myLinkedList.add(3);
        myLinkedList.add(4);
        myLinkedList.add(5);


        myLinkedList.print();
        System.out.println();
//        myLinkedList.remove(4);
        myLinkedList.add(5, 10);

        myLinkedList.print();
        System.out.println();
        System.out.println(myLinkedList.get(0)); //10
        System.out.println(myLinkedList.get(1)); //1
        System.out.println(myLinkedList.get(2)); //2
        System.out.println(myLinkedList.get(3)); //3

        System.out.println(myLinkedList.find(10));

//
//        myLinkedList.remove(3);
//        myLinkedList.add(15);
//
//
//        myLinkedList.print();
//        System.out.println();
//
//        myLinkedList.remove(0);
//        myLinkedList.remove(0);
//        myLinkedList.remove(0);
//
//        myLinkedList.print();
//        System.out.println();
//
//        myLinkedList.remove(0);
//
//        myLinkedList.print();
//        System.out.println();
    }
}
