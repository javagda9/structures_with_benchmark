package com.sda.structures.al;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MyArrayList {
    private Object[] array;
    private int size;

    public MyArrayList() {
        this.array = new Object[10];
        this.size = 0;
    }

    public void add(Object newObject) {
        if (array.length <= size) { // wykraczamy poza tablicę
            extendArraySize();
        }
        array[size] = newObject;
        size++;
    }

    public void removeLast() {
//        remove(size - 1);
        size--;
//        array[size] = null;
    }

    public void remove(int indeks) {
        for (int i = indeks; (i < size) && (i < array.length - 1); i++) {
            array[i] = array[i + 1];
        }
        size--;
//        array[size] = null;
    }

    public int size() {
        return size;
    }

    public Object get(int indeks) {
        // czas stały
        return array[indeks];
    }

    public void print() {
//        System.out.println(Arrays.toString(array));
        for (int i = 0; i < size; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    private void extendArraySize() {
        // alokacja tablicy 2 razy wiekszej niz obecna
        Object[] newArray = new Object[array.length * 2];
        // przepisanie elementow
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }

        // nadpisanie starej tablicy nowa
        array = newArray;
    }

    public void add(int index, Object value) {

        // nie powinno być opcji dodania w
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (array.length <= size) { // wykraczamy poza tablicę
            extendArraySize();
        }
        for (int i = size; i >= index; i--) {
            array[i + 1] = array[i];
        }
        array[index] = value;
        size++;
    }

    public int find(Object value) {
        for (int i = 0; i < size; i++) {
            if (array[i] == value) {
                return i;
            }
        }
        // zwrócić wyjątek ElementNotFound()
        return -1;
    }

    public boolean contains(Object value) {
        for (int i = 0; i < size; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        // zwrócić wyjątek ElementNotFound()
        return false;
    }

    public void clear() {
        size = 0;
    }

    public void set(int index, Object value) {
        if (index >= 0 && index < size) {
            array[index] = value;
        }
        // w przeciwnym razie rzucić wyjątek ArrayIndexOutOfBoundsException
    }

    public void addAll(Object... objects) {
        for (int i = 0; i < objects.length; i++) {
            // korzystamy z istniejącej już metody dodania
            add(objects[i]);
        }
    }

    // dodaj na pozycję - void add(int index, Object value)
    // wyszukaj (zwróć indeks) - int find(Object value)
    // sprawdź czy istnieje - boolean contains(Object value)
    // clear - wyczyść tablicę - void clear()
    // ustaw wartość na pozycji - void set(int index, Object value)
}
