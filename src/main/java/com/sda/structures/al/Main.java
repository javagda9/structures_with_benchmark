package com.sda.structures.al;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        ArrayList<Object> arrayList = new ArrayList<Object>();

//        arrayList.add(19, 123);
        System.out.println(arrayList);


        myArrayList.add(1);// 0
        myArrayList.add(2); // 1
        myArrayList.add(3); // 2
        myArrayList.add(4); // 3
        myArrayList.add(5); // 4
        myArrayList.add(6); // 5
        myArrayList.add(7);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.print();
        myArrayList.add(4);
        myArrayList.print();
        myArrayList.remove(4);
        myArrayList.print();
        myArrayList.remove(4);
        myArrayList.print();
        myArrayList.add(5);
        myArrayList.add(6);
        myArrayList.add(0, 23);
        myArrayList.add(5, 23);

        myArrayList.add(7);
        myArrayList.print();
//        arrayList.add(123);


        System.out.println(myArrayList.find(6));
        System.out.println(myArrayList.find(10));
        System.out.println(myArrayList.contains(6));
        System.out.println(myArrayList.contains(10));
        myArrayList.print();
        myArrayList.clear();
        myArrayList.print();
        myArrayList.add(13);
        myArrayList.print();
        myArrayList.add(14);
        myArrayList.add(16);
        myArrayList.print();

    }
}
