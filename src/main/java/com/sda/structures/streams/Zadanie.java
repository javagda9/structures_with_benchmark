package com.sda.structures.streams;

import java.util.Stack;

public class Zadanie {
    private String name;
    private Status status;
    private Priority priority;
    private int punkty;

    public Zadanie(String name, Status status, Priority priority, int punkty) {
        this.name = name;
        this.status = status;
        this.priority = priority;
        this.punkty = punkty;
    }

    public int getPunkty() {
        return punkty;
    }

    public void setPunkty(int punkty) {
        this.punkty = punkty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }
}
