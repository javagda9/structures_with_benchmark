package com.sda.structures.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamingMain {
    public static void main(String[] args) {
        List<Zadanie> zadania = new ArrayList<>();

        zadania.add(new Zadanie("a", Status.NIEWYKONANE, Priority.LOW, 1));
        zadania.add(new Zadanie("ab", Status.NIEWYKONANE, Priority.HIGH, 2));
        zadania.add(new Zadanie("acc", Status.NIEWYKONANE, Priority.HIGH, 3));
        zadania.add(new Zadanie("addd", Status.WYKONANE, Priority.LOW, 5));
        zadania.add(new Zadanie("aeee", Status.WYKONANE, Priority.HIGH, 10));
        zadania.add(new Zadanie("xaawdaa", Status.NIEWYKONANE, Priority.LOW, 0));

        List<Zadanie> wykonane = new ArrayList<>();
        for (Zadanie z : zadania) {
            if (z.getStatus() == Status.WYKONANE) {
                wykonane.add(z);
            }
        }

//        List<Zadanie> odfiltrowane = zadania.stream().filter(new Predicate<Zadanie>() {
//            @Override
//            public boolean test(Zadanie z) {
//                return z.getStatus() == Status.WYKONANE;
//            }
//        }).collect(Collectors.toList());


        List<Zadanie> odfiltrowane = zadania.stream()
                .filter((z) -> z.getStatus() == Status.WYKONANE)
                .collect(Collectors.toList());

        // stream który jest kolekcją zadań
//        int sumaPunktów = zadania.stream()
//                .map(new Function<Zadanie, Integer>() {
//                    @Override
//                    public Integer apply(Zadanie zadanie) {
//                        return zadanie.getPunkty();
//                    }
//                    // zbiór integerów
//                }).mapToInt(value -> value).sum();


        int sumaPunktów = zadania.stream()
                .map(zadanie -> zadanie.getPunkty())
                .mapToInt(value -> value).sum();

        int sumaPunktów2 = zadania.stream()
                .mapToInt(zadanie -> zadanie.getPunkty())
                .sum();

    }
}
